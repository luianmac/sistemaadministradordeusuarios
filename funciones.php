<?php
function validarContrasena($var1,$var2){
    if(strcmp($var1,$var2) !== 0){
        return false;
    }else{
        return true;
    }
}

function isMail($email){
    if(filter_var($email, FILTER_VALIDATE_EMAIL)){
        return true;
    }else{
        return false;
    }
}

function isEmpty($nombre,$apellido,$usuario,$correo,$contrasena,$confContrasena){
    if(strlen(trim($nombre)) < 1 || strlen(trim($apellido)) < 1 ||
    strlen(trim($usuario)) < 1 ||strlen(trim($correo)) < 1 ||
    strlen(trim($contrasena)) < 1 ||strlen(trim($confContrasena)) < 1 ){
        return true;
    }else{
        return false;
    }
}

function usuarioExiste($usuario){
    global $mysqli;

    $stmt = $mysqli->prepare("SELECT id FROM usuarios WHERE usuario = ? LIMIT 1");
    $stmt->bind_param("s",$usuario);
    $stmt->execute();
    $stmt->store_result();
    $num = $stmt->num_rows;
    $stmt->close();
    if($num>0){
        return true;
    }else{
        return false;
    }
}

function encriptarContrasena($contrasena){
    $pass_e = sha1($contrasena);
    return $pass_e;
}

function registrarUsuario($nombre,$apellido,$usuario,$correo,$pass_c,$tipo_usuario){
    global $mysqli;

    $stmt = $mysqli->prepare("INSERT INTO usuarios (usuario,password,nombre,apellido,correo,tipo_usuario) VALUES (?,?,?,?,?,?)");
    $stmt->bind_param('sssssi',$usuario,$pass_c,$nombre,$apellido,$correo,$tipo_usuario);
    if($stmt->execute()){
        return $mysqli->insert_id;
    }else{
        return 0;
    }
}

function resultBlock($errors){
    if(count($errors)>0){
        echo "<div id='error' class='alert alert-danger' role='alert'>
        <a href='#' onclick=\"showHide('error');\">[x]</a>
        <ul>";
        foreach($errors as $error){
            echo "<li>".$error."</li>";
        }
        echo "</ul>";
        echo "</div>";
    }
}
?>
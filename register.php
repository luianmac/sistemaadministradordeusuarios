<?php
    require "conexion.php";
    require "funciones.php";
    session_start();
    $errors = array();
    if(!empty($_POST)){
        $nombre = $mysqli->real_escape_string($_POST['nombre']);
        $apellido = $mysqli->real_escape_string($_POST['apellido']);
        $usuario = $mysqli->real_escape_string($_POST['usuario']);
        $correo = $mysqli->real_escape_string($_POST['correo']);
        $contrasena = $mysqli->real_escape_string($_POST['contrasena']);
        $confContrasena = $mysqli->real_escape_string($_POST['confContrasena']);

        $tipo_usuario = 2;

        if(isEmpty($nombre,$apellido,$usuario,$correo,$contrasena,$confContrasena)){
            $errors[] = "Todos los campos deben estar llenos";
        }

        if(!isMail($correo)){
            $errors[] = "Direccion de correo Invalida";
        }

        if(!validarContrasena($contrasena,$confContrasena)){
            $errors[] = "Las contraseñas no coinciden";
        }

        if(count($errors)==0){
            $pass_c = encriptarContrasena($contrasena);
            $registro = registrarUsuario($nombre,$apellido,$usuario,$correo,$pass_c,$tipo_usuario);
            if($registro > 0){
                echo "se ha registrado con exito";
                echo "<br><a href='index.php' >Iniciar Sesion</a>";
                exit;
            }else{
                $errors = "Error al registrar";
                
            }
        }


    }
?>
<!DOCTYPE html>
<html lang="es">
    <head>
        <meta charset="utf-8" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge" />
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
        <meta name="description" content="" />
        <meta name="author" content="" />
        <title>Register - SB Admin</title>
        <link href="css/styles.css" rel="stylesheet" />
        <script src="https://use.fontawesome.com/releases/v6.1.0/js/all.js" crossorigin="anonymous"></script>
    </head>
    <body class="bg-primary">
        <div id="layoutAuthentication">
            <div id="layoutAuthentication_content">
                <main>
                    <div class="container">
                        <div class="row justify-content-center">
                            <div class="col-lg-7">
                                <div class="card shadow-lg border-0 rounded-lg mt-5">
                                    <div class="card-header"><h3 class="text-center font-weight-light my-4">Crear Cuenta</h3></div>
                                    <div class="card-body">
                                        <form method="POST" action="<?php echo $_SERVER['PHP_SELF']; ?>">
                                            <div class="row mb-3">
                                                <div class="col-md-6">
                                                    <div class="form-floating mb-3 mb-md-0">
                                                        <input class="form-control" name="nombre" id="inputFirstName" type="text" placeholder="Ingrese su nombre" />
                                                        <label for="inputFirstName">Nombre</label>
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="form-floating">
                                                        <input class="form-control" name="apellido" id="inputLastName" type="text" placeholder="Ingrese su apellido" />
                                                        <label for="inputLastName">Apellido</label>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row mb-3">
                                                <div class="col-md-6">
                                                    <div class="form-floating mb-3 mb-md-0">
                                                        <input class="form-control" name="usuario" id="inputUsuario" type="text" placeholder="Ingrese su usuario" />
                                                        <label for="inputUsuario">Usuario</label>
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="form-floating">
                                                        <input class="form-control" name="correo" id="inputEmail" type="email" placeholder="name@example.com" />
                                                        <label for="inputEmail">Correo</label>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row mb-3">
                                                <div class="col-md-6">
                                                    <div class="form-floating mb-3 mb-md-0">
                                                        <input class="form-control" name="contrasena" id="inputPassword" type="password" placeholder="Crear contraseña" />
                                                        <label for="inputPassword">Contraseña</label>
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="form-floating ">
                                                        <input class="form-control" name="confContrasena" id="inputConfPassword" type="password" placeholder="Repita contraseña" />
                                                        <label for="inputConfPassword">Confimar Contraseña</label>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="mt-4 mb-0">
                                                <div class="d-grid"><button type="submit" class="btn btn-primary btn-block" >Registrar</button></div>
                                            </div>
                                        </form>
                                        <?php echo resultBlock($errors); ?>
                                    </div>
                                    <div class="card-footer text-center py-3">
                                        <div class="small"><a href="login.html">Have an account? Go to login</a></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </main>
            </div>
            <div id="layoutAuthentication_footer">
                <footer class="py-4 bg-light mt-auto">
                    <div class="container-fluid px-4">
                        <div class="d-flex align-items-center justify-content-between small">
                            <div class="text-muted">Copyright &copy; Your Website 2022</div>
                            <div>
                                <a href="#">Privacy Policy</a>
                                &middot;
                                <a href="#">Terms &amp; Conditions</a>
                            </div>
                        </div>
                    </div>
                </footer>
            </div>
        </div>
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js" crossorigin="anonymous"></script>
        <script src="js/scripts.js"></script>
    </body>
</html>

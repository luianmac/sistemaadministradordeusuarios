<?php
    session_start();
    session_destroy();
    header("Location: index.php"); //una vez cerrada la session no permite regresar a pag anterior
?>